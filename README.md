# MindCodingUtils

Utilities for Set UIKit faster.

the following xcode project contains the following functions:

* setBackgroundColor(to component: UIView, color:UIColor)
* setBackgroundImage(to component: UIView, image:UIImage, bounds:CGRect?)
* setTextColor(to component: UIView, color:UIColor)
* setBorder(to component: UIView, color:UIColor?,width:Float?,radius:Float?)
* setFont(to component: UIView, font UFont?, withSize size: Float, systemFont Bool=false,weight:Float?)
* setOpacity(to component:UIView, alpha:Float)
* setShadow(to component:UIVIew, offset:CGPoint?,radius:Float,opacity:Float,color:UIColor?) setIcon(to component:UIView, image:UIImage, bound:CGRect?)
* setLineHeight(to component:UIView, value:)
* alignText(in component:UIView, alignment:<define[left,right,center, justify]>); transformText(in component:UIView, value:<define[uppercased,lowercased,capitalized]>);

# Can edit...

You can edit the name of the Utils class, notice that "Utils" class name its default.

# How to use

One case its the following:
```
Utils.transformText(in: UILabel(), value: .capitalized)
```
