//
//  Utils.swift
//  MindCodingUtilities
//
//  Created by Mervin Flores on 11/21/19.
//  Copyright © 2019 Mervin Flores. All rights reserved.
//

import Foundation
import UIKit

enum StringType{
    case uppercased
    case lowercased
    case capitalized
}

class Utils{
    
    ///Set background color to a specific UIView component
    static func setBackgroundColor(to component: UIView, color:UIColor){ component.backgroundColor = color }
    
    ///Set opacity to a specific UIView component
    static func setOpacity(to component:UIView, alpha:Float){ component.alpha = CGFloat(alpha) }
    
    ///Set height to a specific UIView component
    static func setLineHeight(to component:UIView, value:Float){
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = CGFloat(value)
        
        if component is UILabel{
            if let label = component as? UILabel{
                let attributedString = NSMutableAttributedString(string: label.text!)
                attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
                label.attributedText = attributedString
            }
        } else if component is UITextField{
            if let textField = component as? UITextField{
                let attributedString = NSMutableAttributedString(string: textField.text!)
                attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
                textField.attributedText = attributedString
            }
        } else if component is UITextView{
            if let textView = component as? UITextView{
                let attributedString = NSMutableAttributedString(string: textView.text!)
                attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
                textView.attributedText = attributedString
            }
        } else if component is UIButton{
            if let button = component as? UIButton{
                let attributedString = NSMutableAttributedString(string: button.titleLabel!.text!)
                attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
                button.titleLabel?.attributedText = attributedString
            }
        }
    }
    
    ///Set background image to a specific UIView component
    static func setBackgroundImage(to component: UIView, image:UIImage, bounds:CGRect?){
        
        UIGraphicsBeginImageContext(component.frame.size)
        image.draw(in: bounds ?? component.bounds)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let imageView = UIImageView.init(image: image)
        
        component.addSubview(imageView)
        component.sendSubviewToBack(imageView)
    }
    
    ///Set text color to a specific UIView component
    static func setTextColor(to component: UIView, color:UIColor){
        if component is UILabel{
            if let label = component as? UILabel{
                label.textColor = color
            }
        } else if component is UITextField{
            if let textField = component as? UITextField{
                textField.textColor = color
            }
        } else if component is UITextView{
            if let textView = component as? UITextView{
                textView.textColor = color
            }
        } else if component is UIButton{
            if let button = component as? UIButton{
                button.titleLabel?.textColor = color
            }
        }
    }
    
    ///Set border to a specific UIView component
    static func setBorder(to component: UIView, color:UIColor?,width:Float?,radius:Float?){
        if color != nil{
            component.layer.borderColor = color!.cgColor
        }
        
        if width != nil{
            component.layer.borderWidth = CGFloat(width!)
        }
        
        if radius != nil{
            component.layer.cornerRadius = CGFloat(radius!)
        }
    }
    
    ///Set text font to a specific UIView component
    static func setFont(to component: UIView, font: UIFont?, withSize size: Float, systemFont: Bool = false, weight:Float?){
        
        var customFont = font
        
        if customFont == nil {
            if weight != nil {
                customFont = UIFont.systemFont(ofSize: CGFloat(size), weight: UIFont.Weight(CGFloat(weight!)))
            } else {
                customFont = UIFont.systemFont(ofSize: CGFloat(size))
            }
        }
        
        if component is UILabel{
            if let label = component as? UILabel{
                label.font = customFont?.withSize(CGFloat(size))
            }
        } else if component is UITextField{
            if let textField = component as? UITextField{
                textField.font = customFont?.withSize(CGFloat(size))
            }
        } else if component is UITextView{
            if let textView = component as? UITextView{
                textView.font = customFont?.withSize(CGFloat(size))
            }
        } else if component is UIButton{
            if let button = component as? UIButton{
                button.titleLabel?.font = customFont?.withSize(CGFloat(size))
            }
        }
    }
    
    ///Text alignment to a specific UIView component
    static func alignText(in component:UIView, alignment: NSTextAlignment){
        if component is UILabel{
            if let label = component as? UILabel{
                label.textAlignment = alignment
            }
        } else if component is UITextField{
            if let textField = component as? UITextField{
                textField.textAlignment = alignment
            }
        } else if component is UITextView{
            if let textView = component as? UITextView{
                textView.textAlignment = alignment
            }
        } else if component is UIButton{
            if let button = component as? UIButton{
                button.titleLabel?.textAlignment = alignment
            }
        }
    }
    
    ///Set shadow to a specific UIView component
    static func setShadow(to component:UIView, offset:CGSize?, radius:Float, opacity:Float, color:UIColor?){
        
        if let shadowColor = color {
            component.layer.shadowColor = shadowColor.cgColor
        }
        
        if let shadowOffset = offset{
            component.layer.shadowOffset = shadowOffset
        }
        
        component.layer.shadowRadius = CGFloat(radius)
        component.layer.shadowOpacity = opacity
        
    }
    
    ///Set an icon to a specific UIView component
    static func setIcon(to component:UIView, image:UIImage, bound:CGRect?){
        let imageView = UIImageView.init(image: image)
        
        if let bounds = bound{
            imageView.bounds = bounds
        }
        
        component.addSubview(imageView)
    }
    
    ///Transform text for a specific UIView component
    static func transformText(in component:UIView, value: StringType){
        if component is UILabel{
            if let label = component as? UILabel{
                label.text = self.transformText(textString: label.text!, value: value)
            }
        } else if component is UITextField{
            if let textField = component as? UITextField{
                textField.text = self.transformText(textString: textField.text!, value: value)
            }
        } else if component is UITextView{
            if let textView = component as? UITextView{
                textView.text = self.transformText(textString: textView.text, value: value)
            }
        } else if component is UIButton{
            if let button = component as? UIButton{
                button.titleLabel?.text = self.transformText(textString: button.titleLabel!.text!, value: value)
            }
        }
    }
    
    private static func transformText(textString: String, value: StringType) -> String{
        switch value{
        case .uppercased:
            return textString.uppercased()
        case .lowercased:
            return textString.lowercased()
        case .capitalized:
            return textString.capitalized
        }
    }
    
}
